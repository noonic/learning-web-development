<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo (Helper::sessionGet('user') !== null) ? URL_WEB.'projects.php': URL_WEB; ?>">
                <?php echo $viewData['sitename']; ?>
                <small><?php echo $viewData['version']; ?></small>
            </a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <?php if(Helper::sessionGet('user') === null) { ?>
                <li><a href="<?php echo URL_WEB ?>index.php">Login</a></li>
                <li><a href="<?php echo URL_WEB ?>index.php?action=signup">Signup</a></li>
            <?php } else { ?>
                <li><a href="<?php echo URL_WEB ?>projects.php">Projects</a></li>
                <li>
                    <a href="<?php echo URL_WEB ?>index.php?action=profile_view">
                        <img src="<?php echo URL_IMAGES.Helper::sessionGet('user')['photo']; ?>" alt="profile image" style="height: 20px;" class="img-circle"/>
                        <?php echo Helper::sessionGet('user')['name']; ?>
                    </a>
                </li>
                <li><a href="<?php echo URL_WEB ?>index.php?action=logout">Logout</a></li>
            <?php } ?>
        </ul>
    </div>
</nav>