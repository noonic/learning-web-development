<?php
final class Helper {

    /*
     * Redirects to some URL
     */
    public static function redirect($path = '') {
        $path = ($path == '') ? 'index.php' : $path;
        header('Location: '.URL_WEB.$path);
    }

    /*
     * Clean string
     */
    public static function clean($string) {
        $string = trim($string);
        $string = stripslashes($string);
        $string = htmlspecialchars($string);
        return $string;
    }

    /*
     * Display status message based on status field in the table
     */
    public static function statusDisplay($status) {
        $statusString = intval($status) === 0 ? 'Incomplete' : 'Complete';
        return $statusString;
    }

    /*
     * Session Get
     */
    public static function sessionGet($key) {
        return (isset($_SESSION[$key]) ? $_SESSION[$key] : null);
    }

    /*
     * Session Set
     */
    public static function sessionSet($key, $value) {
        $_SESSION[$key] = $value;
        return $value;
    }

    /*
     * Messages Display
     */
    public static function messagesDisplay($key) {
        $value = self::sessionGet($key);
        self::sessionSet($key, null);
        return $value;
    }

    /*
     * Set Error Messages
     */
    public static function messagesSetError($messages) {
        self::sessionSet('messages_error', $messages);
        return $messages;
    }

    /*
     * Set Success Messages
     */
    public static function messagesSetSuccess($messages) {
        self::sessionSet('messages_success', $messages);
        return $messages;
    }

    public static function authenticateUser() {
        $userDetails = array();
        if(Helper::sessionGet('user') === null) {
            // Set error message
            Helper::messagesSetError(array('Please login to access this page.'));

            // Redirect to login page
            Helper::redirect('index.php');
        } else {
            $userDetails = Helper::sessionGet('user');
        }
        return $userDetails;
    }

    /*
     * Send JSON response to client
     */
    public static function responseJson($response) {
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
}