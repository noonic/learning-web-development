<?php
require_once('includes/common.php');
require_once('views/common/header_view.php');

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'default';

switch($action) {
    case 'save':
        $employeeObject = new Employee();
        $employeeData = array();
        $employeeData['name'] = isset($_POST['employee_name']) ? $_POST['employee_name'] : '';
        $employeeId = isset($_POST['employee_id']) ? $_POST['employee_id'] : 0;
        if($employeeId > 0) { // Edit
            $employeeData['id'] = $employeeId;
            $employeeObject->edit($employeeData);
        } else { // Add
            $employeeObject->add($employeeData);
        }
        Helper::redirect('index.php');
        break;

    case 'edit':
        $employeeId = isset($_GET['id']) ? $_GET['id'] : 0;
        if($employeeId > 0) {
            $employeeObject = new Employee();
            $viewData['employee'] = $employeeObject->getEmployee($employeeId);
        } else {
            Helper::redirect('index.php');
        }
        $view = 'edit_view';
        break;

    case 'add':
        $view = 'add_view';
        break;

    case 'listing':
    default:
        $employeeObject = new Employee();
        $viewData['employees'] = $employeeObject->getList();
        $view = 'listing_view';
}
include('views/index/'.$view.'.php');
include('views/common/footer_view.php');
