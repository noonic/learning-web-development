<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand">EMPLOYEE OPERATIONS</a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo URL_WEB; ?>index.php">All Employees</a></li>
            <li><a href="<?php echo URL_WEB; ?>index.php?action=add">Add New Employee</a></li>
        </ul>
    </div>
</nav>