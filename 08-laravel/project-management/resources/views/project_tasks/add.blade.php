@extends('app')

@section('content')

    <h1>Add new Tasks</h1>

    <hr />
    <div class="col-md-10 col-md-offset-1">
        {!! Form::open(['url' => route ('tasks_save',['projectId' => $projectId]) ]) !!}
        {!! Form::hidden('project_id',$projectId) !!}
        @include('projects._form')

        {!! Form::close() !!}
    </div>

@stop