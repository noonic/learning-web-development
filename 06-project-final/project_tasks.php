<?php
require_once('includes/common.php');

// Check for login
$userDetails = Helper::authenticateUser();

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'default' ;
switch($action) {

    case 'add':
        $view = 'add';
        break;

    case 'edit':
        $id = isset($_GET['id']) ? intval($_GET['id']) : 0 ;
        $userId = Helper::sessionGet('user')['id'];
        $taskObject = new Tasks();
        $viewData['tasks'] = $taskObject->getItem($id,$userId);

        $view = 'edit';
        break;

    case 'save':
        $task = array();
        $task['id'] = isset($_POST['id']) ? Helper::clean($_POST['id']) : 0; // Only in case of EDIT
        $task['project_id'] = isset($_POST['project_id']) ? Helper::clean($_POST['project_id']) : 0 ;
        $task['title'] = isset($_POST['title']) ? Helper::clean($_POST['title']) : '';
        $task['description'] = isset($_POST['description']) ? Helper::clean($_POST['description']) : '';
        $task['status'] = isset($_POST['status']) ? Helper::clean($_POST['status']) : '';
        $task['created_by'] = Helper::sessionGet('user')['id'];

        //validation
        $message = array();
        $message = Validation::validateProjectOrTaskForm($task);
        if($message) {
            Helper::messagesSetError($message);
            Helper::redirect('project_tasks.php?action=add&project_id='.$task['project_id']);
        } else {
            $taskObject = new Tasks();
            $response = $taskObject->saveTask($task);
            Helper::messagesSetSuccess(array('Successful.'));
            Helper::redirect('project_tasks.php?project_id='.$task['project_id']);
        }
        break;

    case 'delete':
        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
        if($id > 0) {
            $taskObject = new Tasks();
            $response = $taskObject->deleteTask($id);
        }
        Helper::redirect('project_tasks.php?project_id='.(intval($_GET['project_id'])));
        break;

    default:
        $id = isset($_GET['project_id']) ? intval($_GET['project_id']) : 0;
        Helper::sessionSet('project_id',$id);
        $userId = Helper::sessionGet('user')['id'];
        $taskObject = new Tasks();
        $viewData['tasks'] = $taskObject->getList($id,$userId);
        $view = 'list';
        break;
}

include(PATH_DEPLOY.'views/project_tasks/'.$view.'.php');