var checkName = /^[A-Za-z ]{3,20}$/;
var checkDate = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
var checkEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
var checkPassword =  /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/;
function validate() {
    var name = document.getElementById("name").value;
    var dob = document.getElementById("dob").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var conPassword = document.getElementById("conpword").value;
    var message = "";
    var status = 1;

    if( ! checkName.test(name)) {
        message = message.concat("Invalid name \n");
        status = 0;
    }

    if(!checkDate.test(dob)) {
        message = message.concat("Invalid date of birth\n");
        status = 0;
    }

    if(!checkEmail.test(email)) {
        message = message.concat("Invalid email\n");
        status = 0;
    }

    if(!checkPassword.test(password)) {
        message = message.concat("Invalid password\n");
        status = 0;
    }

    if(!checkPassword.test(conPassword)) {
        if(password.isEqual(conPassword)) {
            message = message.concat("The re entered password does not match\n");
            status = 0;
        }
    }
    if(status === 0) {
        alert(message);
        return false;
    }  else {
        return true;
    }
}