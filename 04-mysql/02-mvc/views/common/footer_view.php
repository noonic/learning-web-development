<footer class="footer">
    <hr/>

    <div class="container">
        <p class="text-muted text-center">&copy; <?php echo date('Y'); ?></p>
    </div>
</footer>