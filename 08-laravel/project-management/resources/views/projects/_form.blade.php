<div class="form-group">
    {!! Form::label('title','Title:') !!}
    {!! Form::text('title',null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description','Description:') !!}
    {!! Form::textarea('description',null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('status','Status:') !!}
    {!! Form::select('status',['0' => 'Incomplete' , '1' => 'Complete'],null,['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit('Submit',['class' => 'btn btn-success form-control']) !!}
</div>