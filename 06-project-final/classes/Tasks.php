<?php

class Tasks extends Database
{
    private $table = 'project_tasks';

    /*
     * Get list of tasks for a particular project
     */

    public function getList($project_id, $user_id) {
        $tasks = array();

        $queryString = 'SELECT * FROM '.$this->table.' WHERE project_id='.$project_id.' AND created_by='.$user_id;
        $result = $this->select($queryString);
        if($result) {
            $tasks = $result;
        }
        return $tasks;
    }

    /*
     * Delete a task based on task id
     */

    public function deleteTask($id = 0) {
        $response = false;

        $id = intval($id);
        if($id > 0) {
            $queryString = 'DELETE FROM '.$this->table.' WHERE id = '.$id;
            $result = $this->query($queryString);
            if($result) {
                $response = true;
            }
        }

        return $response;
    }

    /*
     * Get a particular task based on task id
     */

    public function getItem($id = 0) {
        $task = array();

        $id = intval($id);
        if($id > 0) {
            $queryString = 'SELECT * FROM '.$this->table.' WHERE id = '.$id;
            $result = $this->select($queryString);
            if($result && isset($result[0])) {
                $task = $result[0];
            }
        }

        return $task;
    }

    /*
     * Save a task in the case of EDIT and ADD
     */

    public function saveTask($task = array()) {
        $response = false;

        if(
            isset($task['title']) &&
            isset($task['description']) &&
            isset($task['status']) &&
            isset($task['created_by']) &&
            isset($task['project_id'])
        ) {
            if($task['id'] && $task['id'] > 0) {
                // EDIT
                $queryString = 'UPDATE '.$this->table;
                $queryString .= ' SET title = "'.$task['title'].'", description = "'.$task['description'].'", status = '.$task['status'].', updated_at = NOW()';
                $queryString .= ' WHERE id = '.$task['id'].' AND project_id='.$task['project_id'];
                $queryString .= ' LIMIT 1';
            } else {
                // ADD
                $queryString = 'INSERT INTO '.$this->table;
                $queryString .= ' (project_id, title, description, status, created_by, created_at, updated_at) ';
                $queryString .= ' VALUES('.$task['project_id'].',"'.$task['title'].'", "'.$task['description'].'", '.$task['status'].', '.$task['created_by'].', NOW(), NOW()) ';
            }
            $result = $this->query($queryString);
            if($result) {
                $response = true;
            }
        }
        return $response;
    }
}