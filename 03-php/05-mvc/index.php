<?php
require_once('common/config.php');

$view = 'views/index/';
$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ( $action){
    case 'about':
        $view .= 'about';
        break;

    case 'signup':
        $view .= 'signup';
        break;

    default:
        $view .= 'login';
        break;

}

include_once(DEPLOY_PATH .  $view . '.php');