<?php

$x = 43;
$y = 7;

echo '$x = ' . $x . '<br> $y = ' . $y ;
echo "<br>Arithmetic Operators <br>";
echo "<br>Addition = ". ($x + $y);
echo "<br>Subtraction = ". ($x - $y);
echo "<br>Multiplication = ". ($x * $y);
echo "<br>Division = ". ($x / $y);
echo "<br>Modulus = ". ($x % $y);

echo "<br>Comparison Operators <br>";

echo "<br>Equal/Identical = var_dump( $x === $y )";
echo var_dump($x === $y);
echo "<br>Unequal/Unidentical = var_dump($x !== $y)";
echo var_dump($x !== $y);
echo "<br>Greater than =  ($x > $y)";
echo var_dump($x > $y);
echo "<br>Lesser than  ($x < $y)";
echo var_dump($x < $y);
echo "<br>Greater than/equal to ($x >= $y)";
echo var_dump($x >= $y);
echo "<br>Lesser than/equal to  ($x <= $y)";
echo var_dump($x <= $y);

echo "<br>Increment/Decrement Operators <br>";
echo "<br> Increment x". ++$x;
echo " <br>Decrement x". --$y;

echo "<br>Logical Operators <br>";
echo "<br>And/&& - 0 and $y  ";
echo var_dump(0 and $y);
echo "<br>Or/|| - 0 or $y  ";
echo var_dump(0 or $y);
echo "<br>Or - $x xor $y  ";
echo var_dump($x xor $y);
echo "<br>Not - !$x  ";
echo var_dump(!$x);

echo "<br><br>String Operators <br>";
$str1="Hello";
$str2="World";
echo "<br>str1=$str1 <br> str2=$str2 ";
echo "<br>Concatenation operator ".'$str1 . $str2 '. " =$str1 . $str2";
$str1.=$str2;
echo "<br>Concatenation assignment operator ".'$str1 .= $str2 '.$str1 ;

echo "<br><br>Array Operators <br>";
$x = array("a" => "red", "b" => "green");
$y = array("c" => "blue", "d" => "yellow");

echo "<br>Concatenation operator".'  $x+$y = ';
print_r($x + $y);

echo "<br>Equality operator".'  $x == $y = ';
echo var_dump($x == $y);

echo "<br>Identity operator".'  $x === $y = ';
echo var_dump($x === $y);

echo "<br>Inequality operator".'  $x != $y = ';
echo var_dump($x != $y);

echo "<br>Non-identity operator".'  $x !== $y = ';
echo var_dump($x !== $y);
