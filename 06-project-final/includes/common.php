<?php
require_once('includes/config.php');

//SESSION START
session_start();

spl_autoload_register(function ($class) {
    include PATH_DEPLOY.'classes/'.$class.'.php';
});

$viewData = array();
$viewData['sitename'] = 'Project Management';
$viewData['version'] = '1.0v';
$viewData['profile'] = array();

$page= '';

register_shutdown_function(function() {
    Database::closeDatabaseConnection();
});