<?php

// Paths for structure and views
define('DEPLOY_PATH' , $_SERVER['DOCUMENT_ROOT'] .'03-php/05-mvc/');
define ('WEB_PATH', 'http://' . $_SERVER['HTTP_HOST'] .'/03-php/05-mvc/');

// Paths for the static stuff
define('CSS_PATH','http://'. $_SERVER['HTTP_HOST'] . '/03-php/05-mvc/static/css/' );
define('JS_PATH', 'http://'.$_SERVER['HTTP_HOST'] . '/03-php/05-mvc/static/js/' );
define('IMAGE_PATH','http://'. $_SERVER['HTTP_HOST'] . '/03-php/05-mvc/static/images/' );
