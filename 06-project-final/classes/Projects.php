<?php

class Projects extends Database {

    private $table = 'projects';

    /*
     * Get list of projects
     */

    public function getList($userId = 0) {
        $projects = array();

        $userId = intval($userId);
        if($userId > 0) {
            $queryString = 'SELECT * FROM ' . $this->table . ' WHERE created_by = ' . $userId;
            $result = $this->select($queryString);
            if ($result) {
                $projects = $result;
            }
        }

        return $projects;
    }

    /*
     * Delete a project by project id
     */

    public function deleteProject($id = 0) {
        $response = false;

        $id = intval($id);
        if($id > 0) {
            $queryString = 'DELETE FROM '.$this->table.' WHERE id = '.$id;
            $result = $this->query($queryString);
            if($result) {
                $response = true;
            }
        }

        return $response;
    }

    /*
     * Save a project in the case of EDIT and ADD
     */

    public function saveProject($project = array()) {
        $response = false;

        if(
            isset($project['title']) &&
            isset($project['description']) &&
            isset($project['status']) &&
            isset($project['created_by'])
        ) {
            if($project['id'] && $project['id'] > 0) {
                // EDIT
                $queryString = 'UPDATE '.$this->table;
                $queryString .= ' SET title = "'.$project['title'].'", description = "'.$project['description'].'", status = '.$project['status'].', updated_at = NOW()';
                $queryString .= ' WHERE id = '.$project['id'];
                $queryString .= ' LIMIT 1';
            } else {
                // ADD
                $queryString = 'INSERT INTO '.$this->table;
                $queryString .= ' (title, description, status, created_by, created_at, updated_at) ';
                $queryString .= ' VALUES("'.$project['title'].'", "'.$project['description'].'", '.$project['status'].', '.$project['created_by'].', NOW(), NOW()) ';
            }

            $result = $this->query($queryString);
            if($result) {
                $response = true;
            }
        }

        return $response;
    }

    /*
     * Used to get a particular project based on project id
     */

    public function getItem($id = 0) {
        $project = array();

        $id = intval($id);
        if($id > 0) {
            $queryString = 'SELECT * FROM '.$this->table.' WHERE id = '.$id;
            $result = $this->select($queryString);
            if($result && isset($result[0])) {
                $project = $result[0];
            }
        }

        return $project;
    }

}