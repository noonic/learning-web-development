<?php
require_once('includes/common.php');

// Check for login
$userDetails = Helper::authenticateUser();

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'default';
switch($action) {

    case 'save':
        $project = array();
        $project['id'] = isset($_POST['id']) ? Helper::clean($_POST['id']) : 0; // Only in case of EDIT
        $project['title'] = isset($_POST['title']) ? Helper::clean($_POST['title']) : '';
        $project['description'] = isset($_POST['description']) ? Helper::clean($_POST['description']) : '';
        $project['status'] = isset($_POST['status']) ? Helper::clean($_POST['status']) : '';
        $project['created_by'] = Helper::sessionGet('user')['id'];

        $message = array();
        $message = Validation::validateProjectOrTaskForm($project);

        if($message) {
            Helper::messagesSetError($message);
            Helper::redirect('projects.php?action=add');
        } else {
            $projectObject = new Projects();
            $response = $projectObject->saveProject($project);
            Helper::messagesSetSuccess(array('Successful.'));
            Helper::redirect('projects.php');
        }
        break;

    case 'edit':
        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
        $projectObject = new Projects();
        $viewData['project'] = $projectObject->getItem($id);

        $view = 'edit';
        break;

    case 'add':
        $view = 'add';
        break;

    case 'delete':
        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
        if($id > 0) {
            $projectObject = new Projects();
            $response = $projectObject->deleteProject($id);
        }
        Helper::redirect('projects.php');
        break;

    default:
        $projectObject = new Projects();
        $userId = Helper::sessionGet('user')['id'];
        $viewData['projects'] = $projectObject->getList($userId);
        $view = 'list';
        break;
}

include(PATH_DEPLOY.'views/projects/'.$view.'.php');