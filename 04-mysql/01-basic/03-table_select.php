<?php
require_once('common/connection_create.php');

$sqlQuery = "SELECT * FROM projects";
$result = $connection->query($sqlQuery);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title> Profile View</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width="device-width",initial-scale=1">

    <!--Custom CSS-->
    <link rel="stylesheet" href="css/style.css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand">Project Management</a>
            </div>
            <ul>
                <li>
                    <a href="04-project_insert.php" class="pull-right">Add project</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container">
        <table class="table table-hover">
            <caption class="text-center"><h3>Projects</h3></caption>

            <thead>
                <tr>
                    <th rowspan="2" width="7%">Serial No.</th>
                    <th rowspan="2">Title</th>
                    <th rowspan="2">Description</th>
                    <th rowspan="2">Created By</th>
                    <th rowspan="2">Status</th>
                    <th colspan="2" class="text-center">Actions</th>
                </tr>

                <tr>
                    <th> Edit </th>
                    <th> Delete</th>
                </tr>
            </thead>

            <tbody>
                <?php while( $row = $result->fetch_assoc() ) {?>
                <tr>
                    <td><?php echo $row['id']; ?></td>
                    <td><?php echo $row['title']; ?></td>
                    <td><?php echo $row['description']; ?></td>
                    <td><?php echo $row['created_by']; ?></td>
                    <td><?php echo $row['status']; ?></td>
                    <td><a href="05-project_update.php?id=<?php echo $row['id']?>">Edit</a> </td>
                    <td><a href="06-project_delete.php?id=<?php echo $row['id']?>">Delete</a> </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-muted text-center">Custom Footer</p>
        </div>
    </footer>
</body>
</html>

<?php require_once('common/connection_close.php'); ?>