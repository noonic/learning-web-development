<?php
require 'vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

$filename = 'example_files/info.yaml';
$info = Yaml::parse(file_get_contents($filename));

// Debugging using print_r php function
print_r($info);

// Debugging using Yaml
print Yaml::dump($info);

// Debugging using KINT
d($info);