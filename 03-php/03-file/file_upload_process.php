<?php
$targetDirectory = "uploads/";
$targetPath = $targetDirectory . basename($_FILES['upload_file']['name']);

$uploadOK = 1;
$check = getimagesize($_FILES['upload_file']['tmp_name']);
echo $check['mime'];

$imageType = pathinfo( $targetPath, PATHINFO_EXTENSION );

if($imageType != "jpg" && $imageType != "gif" && $imageType != "png") {
    $uploadOK = 0;
}

if($uploadOK === 1){
    move_uploaded_file($_FILES["upload_file"]["tmp_name"], $targetPath);
} else {
    echo "Can't upload file";
}