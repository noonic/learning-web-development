<?php

class User extends Database {

    private $table = 'users';

    /*
     * Get a particular user's profile based on email and password as well as set the session['user_id'] to user's id
     */
    public function getProfile($email , $password) {
        $response = array(
            'user_details' => array(),
            'success' => false,
            'message' => 'Failed'
        );

        if($email !== '' AND $password !== '') {
            $queryString = 'SELECT * FROM '.$this->table.' WHERE email = "'.$email. '" AND password="'. $password. '" LIMIT 1';
            $result = $this->select($queryString);

            if($result && isset($result[0])) {
                $response['user_details'] = $result[0];
                $response['success'] = true;
                $response['message'] = 'You have been signed in successfully';
            } else {
                $response['message'] = 'Incorrect username and password provided.';
            }
        } else {
            $response['message'] = 'Please enter username and password';
        }

        return $response;
    }

    /*
     * get a particular user's profile based on user id
     */
    public function getItem($id) {
        $profile = array();

        $queryString = 'SELECT * FROM '.$this->table.' WHERE id = '.$id.' LIMIT 1';
        $result = $this->select($queryString);

        if($result && isset($result[0])) {
            $profile = $result[0];
        }
        return $profile;
    }

    /*
     * Save a user in the case of EDIT and ADD
     */
    public function saveUser($profile = array()) {
        $response = false;
        if(
            isset($profile['name']) &&
            isset($profile['dob']) &&
            isset($profile['email']) &&
            isset($profile['password']) &&
            isset($profile['photo'])
        ) {
            if($profile['id'] && $profile['id'] > 0) {
                // EDIT
                $queryString = 'UPDATE '.$this->table;
                $queryString .= ' SET name = "'.$profile['name'].'",photo = "'.$profile['photo'].'", dob = "'.$profile['dob'].'", email = "'.$profile['email'].'", password = "'.$profile['password'].'"';
                $queryString .= ' WHERE id = '.$profile['id'];
                $queryString .= ' LIMIT 1';
            } else {
                // ADD
                $queryString = 'INSERT INTO '.$this->table;
                $queryString .= ' (name, photo, dob, email, password) ';
                $queryString .= ' VALUES("'.$profile['name'].'", "'.$profile['photo'].'", "'.$profile['dob'].'", "'.$profile['email'].'", "'.$profile['password'].'")';
            }
            $result = $this->query($queryString);
            if($result) {
                $response = true;
            }
        }
        return $response;
    }

    /*
     * Check email availability
     */
    public function emailAlreadyExists($email = '') {
        $response = false;

        if($email != '' && strlen($email) > 3) {
            $queryString = 'SELECT count(1) as count FROM '.$this->table.' WHERE email = "'.$email.'" LIMIT 1';
            $result = $this->select($queryString);
            if($result && intval($result[0]['count']) === 1) {
                $response = true;
            }
        }

        return $response;
    }
}