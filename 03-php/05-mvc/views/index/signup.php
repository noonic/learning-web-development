<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once('views/common/head.php'); ?>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <?php include_once('views/common/header.php'); ?>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo WEB_PATH;?>index.php"><span class="glyphicon glyphicon-log-in"></span>Logout</a> </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form action="<?php echo WEB_PATH;?>index.php">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" placeholder="Name">
                </div>

                <div class="form-group">
                    <label for="dob">Date of Birth:</label>
                    <input type="date" class="form-control" id="dob" placeholder="Date of birth">
                </div>

                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" placeholder="Email">
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password">
                </div>

                <div class="form-group">
                    <label for="conpword">Password</label>
                    <input type="password" class="form-control" id="conpword" placeholder="Password">
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php include_once('views/common/footer.php')?>
</body>
</html>