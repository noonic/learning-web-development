@extends('app')
@section('content')
<div class="container">
    <h1>
        Edit profile

        <a href="/profile" class="btn btn-default pull-right">Back</a>
    </h1>

    <hr/>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form method="POST" action="/profile" name="form" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                            <!--    <input type="hidden" name="change_password_status" id="change_password_status" value="<?php// echo Helper::sessionGet('user')['id'] ? 0 : 1; ?>"> -->

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ Auth::user()->name }}">
                    </div>

                    <div class="form-group">
                        <label for="dob">Date of Birth:</label>
                        <input type="date" class="form-control" id="dob" name="dob" placeholder="Enter date of birth in YYYY-MM-DD format" value="{{ Auth::user()->dob }}">
                        <p id="p2"></p>
                    </div>

                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ Auth::user()->email }}" />
                        <span id="user-availability-status"></span>
                    </div>

                    <div class="form-group" >
                        <label for="photo">Profile Picture:</label>

                        <br />
                        <img src="/{{Auth::user()->photo}}" alt="Profile pic" style="width:128px;height:128px;">

                        <br />
                        <input type="file" name="photo" id="photo" />
                    </div>

                    <button id="password-inputs-control" class="btn btn-default" type="button" >Change password</button>


                    <div id="password-inputs" style="display:none;">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password(>6 characters and <20 characters)"  autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="confirm_password">Re-enter password</label>
                            <input type="password" class="form-control" id="confirm_password" name="password_confirmation" placeholder="Confirm password"  autocomplete="off">
                        </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-default" >Submit</button>

                </form>
            </div>
        </div>
    </div>
</div>
@stop
