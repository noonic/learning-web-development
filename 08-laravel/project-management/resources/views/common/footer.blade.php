<footer class="footer pull-down">
    <hr/>

    <div class="container">

        <p class="text-muted text-center">
            &copy; <?php echo date('Y'); ?> Project Management.
            &bull;
            <a href="/about">About</a>
            &bull;
            <a href="/contact">Contact</a>
        </p>

    </div>
</footer>