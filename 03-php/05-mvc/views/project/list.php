<!DOCTYPE html>
<html lang="en">
<head>
<head>
    <?php include_once('views/common/head.php'); ?>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <?php include_once('views/common/header.php'); ?>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo WEB_PATH;?>project.php?action=add">Add project+</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <table class="table table-hover">
        <caption class="text-center"><h3>Projects</h3></caption>

        <thead>
        <tr>
            <th rowspan="2" width="7%">Serial No.</th>
            <th rowspan="2">Title</th>
            <th rowspan="2">Description</th>
            <th rowspan="2">Created By</th>
            <th rowspan="2">Status</th>
            <th colspan="2" class="text-center">Actions</th>
        </tr>

        <tr>
            <th> Edit </th>
            <th> Delete</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>1.</td>
            <td ><a href="" >ABC</a></td>
            <td></td>
            <td></td>
            <td></td>

            <td><a href=" <?php echo WEB_PATH;?>project.php?action=edit">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>

        <tr>
            <td>2.</td>
            <td ><a href="" >DEF</a></td>
            <td></td>
            <td></td>
            <td></td>

            <td><a href=" <?php echo WEB_PATH;?>project.php?action=edit">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>

        <tr>
            <td>3.</td>
            <td ><a href="" >GHI</a></td>
            <td></td>
            <td></td>
            <td></td>

            <td><a href=" <?php echo WEB_PATH;?>project.php?action=edit">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>

        <tr>
            <td>4.</td>
            <td ><a href="" >JKL</a></td>
            <td></td>
            <td></td>
            <td></td>

            <td><a href=" <?php echo WEB_PATH;?>project.php?action=edit">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>

        <tr>
            <td>5.</td>
            <td ><a href="" >MNO</a></td>
            <td></td>
            <td></td>
            <td></td>
            <td><a href=" <?php echo WEB_PATH;?>project.php?action=edit">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>

        <tr>
            <td>6.</td>
            <td ><a href="" >PQR</a></td>
            <td></td>
            <td></td>
            <td></td>

            <td><a href=" <?php echo WEB_PATH;?>project.php?action=edit">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>
        </tbody>
    </table>
</div>

<?php include_once('views/common/footer.php')?>
</body>
</html>