<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(PATH_DEPLOY.'views/common/head.php'); ?>
    <title>Edit profile</title>
</head>

<body>
    <!-- Header -->
    <?php include_once(PATH_DEPLOY.'views/common/header.php')?>

    <!-- Content -->
    <div class="container">
        <h1>
            Edit profile

            <a href="<?php echo URL_WEB.'index.php?action=profile_view'; ?>" class="btn btn-default pull-right">Back</a>
        </h1>

        <hr/>

        <?php include_once(PATH_DEPLOY.'views/index/_form.php')?>
    </div>

    <!-- Footer -->
    <?php include_once(PATH_DEPLOY.'views/common/footer.php')?>
</body>
</html>