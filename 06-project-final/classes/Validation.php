<?php

final class Validation
{
   public static function validateSignUp($profile)
   {
       $errorMsg = array();

       $name = $profile['name'];
       $dob = $profile['dob'];
       $email = $profile['email'];
       $password = $profile['password'];
       $confirmPassword = $profile['confirm_password'];
        $i = 0;


       // Full Name
       if (preg_match('/^[A-Za-z ]{3,20}$/', $name) == 0) {
           $errorMsg[$i++]= 'Enter valid Name.';
       }
       // Date
       if (preg_match('/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/', $dob) == 0) {
           $errorMsg[$i++] = 'Enter valid date. ';
       }

       // Email
       if (preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $email) == 0) {
           $errorMsg[$i++] = 'Enter valid Email. ';
       }

        if($profile['change_password_status'] === 1) {
            // Password min 6 char max 20 char
            if (preg_match('/^[A-Za-z0-9!@#$%^&*()_]{3,20}$/', $password) == 0) {
                $errorMsg[$i++] = 'Enter valid Password min 3 Chars. ';
            }

            if ($confirmPassword !== $password) {
                $errorMsg[$i++] = "Re-entered password doesn't match. ";
            }
        }

       return $errorMsg;
   }

    public static function validateProjectOrTaskForm($item){
        $errorMsg = array();
        $title = $item['title'];
        $i = 0;

        if (preg_match('/^[A-Za-z0-9 ]{3,20}$/', $title) === 0) {
            $errorMsg[$i++]= 'Enter valid Title.';
        }
        return $errorMsg;
    }
}