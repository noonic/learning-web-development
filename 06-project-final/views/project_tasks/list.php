<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(PATH_DEPLOY.'views/common/head.php'); ?>
</head>

<body>
    <!-- Header -->
    <?php include_once(PATH_DEPLOY.'views/common/header.php')?>
    <!--Messages -->
    <?php include_once(PATH_DEPLOY.'views/common/messages.php')?>
    <!-- Content -->
    <div class="container">
        <h1>
            Task List

            <a href="<?php echo URL_WEB.'project_tasks.php?action=add&project_id='.Helper::sessionGet('project_id')?>" class="btn btn-default pull-right">Add</a>
        </h1>

        <hr/>

        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Description</th>
                <th>Status</th>
                <th>Created By</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th colspan="2" class="text-center">Actions</th>
            </tr>
            </thead>

            <tbody>
            <?php if(count($viewData['tasks']) > 0) { ?>
                <?php foreach($viewData['tasks'] as $i => $task) { ?>
                    <tr>
                        <td><?php echo ($i+1); ?></td>
                        <td><?php echo $task['title']; ?></td>
                        <td><?php echo $task['description']; ?></td>
                        <td><?php echo (Helper::statusDisplay( $task['status'] )); ?></td>
                        <td><?php echo $task['created_at']; ?></td>
                        <td><?php echo $task['updated_at']; ?></td>
                        <td><a href="<?php echo URL_WEB.'project_tasks.php?action=edit&project_id='.$task['project_id'].'&id='.$task['id']; ?>">Edit</a></td>
                        <td><a href="<?php echo URL_WEB.'project_tasks.php?action=delete&project_id='.$task['project_id'].'&id='.$task['id']; ?>" onclick="return App.confirmDelete();">Delete</a></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="8" align="center">No data found.</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <!-- Footer -->
    <?php include_once(PATH_DEPLOY.'views/common/footer.php')?>
</body>
</html>