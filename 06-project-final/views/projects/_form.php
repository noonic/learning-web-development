<?php

    $id = isset($viewData['project']['id']) ? $viewData['project']['id'] : 0  ;
    $title = isset($viewData['project']['title']) ? $viewData['project']['title'] : '' ;
    $description = isset($viewData['project']['description']) ? $viewData['project']['description'] : '' ;
    $createdBy = isset($viewData['project']['created_by']) ? $viewData['project']['created_by'] : Helper::sessionGet('user')['id'] ;

    $status = isset($viewData['project']['status']) ? intval($viewData['project']['status']) : 0 ;
?>

<form action="<?php echo URL_WEB.'projects.php?action=save'; ?>" method="post" onsubmit="return App.validationActions.projectsAndTasks();">
    <input type="hidden" name="id" value="<?php echo $id; ?>" />

    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $title; ?>" />
    </div>

    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control" name="description" id="description" placeholder="Description" value="<?php echo $description; ?>">
    </div>

    <div class="form-group">
        <label for="status">Status</label>
        <select class="form-control" name="status" id="status">
            <option value="0" <?php echo ($status === 0) ? 'selected="selected"' : '' ?>>Incomplete</option>
            <option value="1" <?php echo ($status === 1) ? 'selected="selected"' : '' ?>>Completed</option>
        </select>
    </div>

    <hr/>

    <button type="submit" class="btn btn-success">Submit</button>
</form>