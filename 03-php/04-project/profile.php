<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once('common/head.php'); ?>
</head>
<body>
<div>
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Project Management</a>

            </div>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="profile_edit.php">Edit Profile</a></li>
                <li><a href="projects.php">View Projects</a></li>
                <li><a href="index.php">Logout</a> </li>
            </ul>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">UserName</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Date of Birth:</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">07/02/1994</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email id:</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">x.yz@noonic.com</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php include_once('common/footer.php')?>
</body>
</html>