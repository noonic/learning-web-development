<?php
class Employee extends Database {
    
    protected $table = 'employee';

    public function getList() {
        $return = array();
        $sql = 'SELECT * from '.$this->table;
        $result = $this->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $return[] = $row;
            }
        }
        return $return;
    }

    public function getEmployee($employeeId) {
        $return = array();
        if($employeeId > 0) {
            $sql = 'SELECT * from '.$this->table.' WHERE id = '.$employeeId.' LIMIT 1';
            $result = $this->query($sql);
            if($result->num_rows > 0) {
                $return = $result->fetch_assoc();
            }
        }
        return $return;
    }

    public function add($employeeData) {
        $return = false;
        if(isset($employeeData['name']) && $employeeData['name'] != '') {
            $sql = 'INSERT INTO '.$this->table.' (name) VALUES("'.$employeeData['name'].'")';
            $return = $this->query($sql);
        }
        return $return;
    }

    public function edit($employeeData) {
        $return = false;
        if(isset($employeeData['id']) && isset($employeeData['name']) && $employeeData['id'] > 0) {
            $sql = 'UPDATE '.$this->table.' SET name = "'.$employeeData['name'].'" WHERE id = '.$employeeData['id'].' LIMIT 1';
            $return = $this->query($sql);
        }
        return $return;
    }

}