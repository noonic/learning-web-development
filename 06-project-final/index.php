<?php
require_once('includes/common.php');

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'default';

switch($action) {

    case 'email_availability':
        $response = array(
            'success' => false,
            'message' => 'Error',
            'available' => false
        );

        $email = isset($_POST['email']) ? Helper::clean($_POST['email']) : '';
        if(!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $userObject = new User();
            $emailAlreadyExists = $userObject->emailAlreadyExists($email);
            if ($emailAlreadyExists) {
                $response['message'] = 'Oops! Email is not available.';
            } else {
                $response['success'] = true;
                $response['message'] = 'Hurray! Email is available.';
                $response['available'] = true;
            }
        } else {
            $response['message'] = 'Not a valid email.';
        }

        Helper::responseJson($response);
        break;

    case 'save':
        $profile = array();

        // 1. collect input
        $page = Helper::sessionGet('user')['id'] ? 'profile_edit' : 'signup';
        $profile['id'] = isset($_POST['id']) ? Helper::clean($_POST['id']) : 0; // Only in case of EDIT
        $profile['name'] = isset($_POST['name']) ? Helper::clean($_POST['name']) : '';
        $profile['dob'] = isset($_POST['dob']) ? Helper::clean($_POST['dob']) : '';
        $profile['email'] = isset($_POST['email']) ? Helper::clean($_POST['email']) : '';
        $profile['password'] = isset($_POST['password']) ? Helper::clean($_POST['password']) : '';
        $profile['confirm_password'] = isset($_POST['confirm_password']) ? Helper::clean($_POST['confirm_password']) : '';
        $profile['change_password_status'] = isset($_POST['change_password_status']) ? intval($_POST['change_password_status']) : 0;
        // Profile Pic
        if(isset($_FILES['photo']) && $_FILES['photo']['error'] === 0) {
            $uploadFolder = 'uploads';
            $extension = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);
            $fileName = md5(Helper::sessionGet('user')['id']) . '.' . $extension;
            $serverFilePath = PATH_UPLOADS . $fileName;

            $profile['photo'] = $uploadFolder . '/' . $fileName;
            if (move_uploaded_file($_FILES['photo']['tmp_name'], $serverFilePath)) {
                $fileMessage = "The file " . $_FILES['photo']['name'] . " has been uploaded, and your information has been added to the directory";
            } else {
                $fileMessage = "Sorry, there was a problem uploading your file.";
            }
        } else {
            $profile['photo'] = (Helper::sessionGet('user')['id']) ? Helper::sessionGet('user')['photo'] : DEFAULT_USER_IMAGE;
        }

        // 2. validate
            $message = array();
            $message = Validation::validateSignUp($profile);
            if($message) { //in case errors exist
                Helper::messagesSetError($message);
                $viewData['profile']['id'] = $profile['id'];
                $viewData['profile']['name'] = $profile['name'];
                $viewData['profile']['dob'] = $profile['dob'];
                $viewData['profile']['email'] = $profile['email'];
                $viewData['profile']['password'] = $profile['password'];
                $viewData['profile']['confirm_password'] = $profile['confirm_password'];
                $viewData['profile']['photo'] = $profile['photo'];

                if( $page === 'signup') {
                    $view = 'signup';
                } else {
                    $view = 'profile_edit';
                }

            } else {
                // 3. save in database
                $profile['password'] = md5($profile['password']);
                $userObject = new User();
                $response = $userObject->saveUser($profile);

                // 4. give user message if error or success
                if ($page === 'profile_edit') {
                    // Update the session, Set user information
                    unset($profile['password']);
                    Helper::sessionSet('user', $profile);
                    // Set success message
                    Helper::messagesSetSuccess(array('Profile was updated successfully.'));

                    Helper::redirect('index.php?action=profile_view');
                } else {
                    // Set success message
                    Helper::messagesSetSuccess(array('You have been registered successfully, please login with your credentials.'));
                    $viewData['profile']['id'] = $profile['id'];
                    $viewData['profile']['name'] = $profile['name'];
                    $viewData['profile']['dob'] = $profile['dob'];
                    $viewData['profile']['email'] = $profile['email'];
                    $viewData['profile']['password'] = $profile['password'];
                    $viewData['profile']['confirm_password'] = $profile['confirm_password'];
                    $viewData['project']['photo'] = $profile['photo'];

                    Helper::redirect('index.php');
                }
            }
        break;

    case 'profile_view':
        $userObject = new User();
        $viewData['profile'] = $userObject->getItem(Helper::sessionGet('user')['id']);
        $view = 'profile_view';
        break;

    case 'profile_edit':
        $userObject = new User();
        $viewData['profile'] = $userObject->getItem(Helper::sessionGet('user')['id']);
        $view = 'profile_edit';
        break;

    case 'about':
        $view = 'about';
        break;

    case 'contact':
        $view = 'contact_us';
        break;

    case 'signup':
        $view = 'signup';
        break;

    case 'logout':
        session_destroy();

        session_start();

        // Set success message
        Helper::messagesSetSuccess(array('You have been logged out successfully.'));

        Helper::redirect('index.php');
        break;

    case 'login_check':
        $userObject = new User();

        $email = isset($_POST['email']) ? ($_POST['email']) : '';
        $password = isset($_POST['password']) ? md5(($_POST['password'])) : '';

        $userDetails = $userObject->getProfile($email, $password);
        if($userDetails['success']) {
            unset($userDetails['user_details']['password']);

            // Set user login information
            Helper::sessionSet('user', $userDetails['user_details']);

            // Set success message
            Helper::messagesSetSuccess(array($userDetails['message']));

            // Redirect user to project page
            Helper::redirect('projects.php');
        } else {
            // Set error message
            Helper::messagesSetError(array($userDetails['message']));

            Helper::redirect('index.php');
        }
        break;

    default:
        $view = 'login';
}

include(PATH_DEPLOY.'views/index/'.$view.'.php');