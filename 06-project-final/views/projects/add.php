<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(PATH_DEPLOY.'views/common/head.php'); ?>
</head>

<body>
    <!-- Header -->
    <?php include_once(PATH_DEPLOY.'views/common/header.php')?>

    <!-- Messages -->
    <?php include_once(PATH_DEPLOY.'views/common/messages.php')?>

    <!-- Content -->
    <div class="container">
        <h1>
            Projects - Add

            <a href="<?php echo URL_WEB.'projects.php'; ?>" class="btn btn-default pull-right">Back</a>
        </h1>

        <hr/>

        <?php include_once(PATH_DEPLOY.'views/projects/_form.php')?>
    </div>

    <!-- Footer -->
    <?php include_once(PATH_DEPLOY.'views/common/footer.php')?>
</body>
</html>