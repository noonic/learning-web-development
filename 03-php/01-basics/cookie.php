<?php

$cookie_name = "sampleCookie";
$cookie_value = "sampleValue";
setcookie($cookie_name, $cookie_value, time() + (60*60*24* 30), "/");
?>
<html>
<body>
    <?php
    if(!isset($_COOKIE[$cookie_name])) {
        echo "Cookie is not set!";
    } else {
        echo "Cookie name:" . $cookie_name . " is set!<br>";
        echo "Value: " . $_COOKIE[$cookie_name];
    }
    ?>
</body>
</html>
