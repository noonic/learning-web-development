<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);
//static pages
Route::get('about', 'IndexController@about');
Route::get('contact', 'IndexController@contact');


// Login page
Route::get('/', 'IndexController@login');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//User related routes
Route::get('profile', 'IndexController@profile');
Route::post('profile', 'IndexController@edit');
Route::get('profile/edit', 'IndexController@profile_edit');

// Projects

Route::get('projects', ['as' => 'projects_lists', 'uses' => 'ProjectsController@lists' ]);
Route::post('projects', ['as' => 'projects_save', 'uses' => 'ProjectsController@save']);
Route::get('projects/add', ['as' => 'projects_add', 'uses' =>'ProjectsController@add']);
Route::get('projects/{projectId}/edit', ['as' => 'projects_edit', 'uses' =>'ProjectsController@edit']);
Route::get('projects/{projectId}/delete', ['as' => 'projects_delete', 'uses' =>'ProjectsController@delete']);
Route::patch('projects/{projectId}', ['as' => 'projects_update', 'uses' => 'ProjectsController@update' ]);



// Projects Tasks
Route::get('projects/{projectId}/tasks', [ 'as' => 'tasks_lists', 'uses' => 'ProjectTasksController@lists' ]);


Route::post('projects/{projectId}/tasks', ['as' => 'tasks_save', 'uses' =>'ProjectTasksController@save']);
Route::get('projects/{projectId}/tasks/{id}/delete', ['as' => 'tasks_delete', 'uses' =>'ProjectTasksController@delete']);

Route::get('projects/{projectId}/tasks/add',['as' => 'tasks_add', 'uses' =>'ProjectTasksController@add'] ); //
Route::patch('projects/{projectId}/tasks/{id}', ['as' => 'tasks_update', 'uses' => 'ProjectTasksController@update' ]);
Route::get('projects/{projectId}/tasks/{id}', ['as' => 'tasks_edit', 'uses' =>'ProjectTasksController@edit']);