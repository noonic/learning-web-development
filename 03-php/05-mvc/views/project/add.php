<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once('views/common/head.php'); ?>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <?php include_once('views/common/header.php'); ?>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo WEB_PATH;?>project/list.php">Back</a> </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form action="<?php echo WEB_PATH;?>project.php">
                <h2>Enter the following fields:</h2>

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" id="title" placeholder="Title">
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea class="form-control" id="description" placeholder="Description"></textarea>
                </div>


                <div class="form-group">
                    <label for="createdby">Created by</label>
                    <input type="text" class="form-control" id="createdby" placeholder="UserName" readonly>
                </div>

                <div class="form-group">
                    <label for="status">Status</label>
                    <select id="status" class="form-control">
                        <option value="incomplete" style="background:red">Incomplete</option>
                        <option value="completed" style="background:green">Completed</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php include_once('views/common/footer.php')?>
</body>
</html>