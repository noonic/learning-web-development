@extends('app')

@section('content')



    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-4">
                <form class="form-horizontal">
                    <div class="form-group" >
                        <label class="col-sm-2 control-label">Profile Picture:</label>

                        <div class="col-sm-10">
                            <img src="{{Auth::user()->photo}}" alt="Profile pic" style="width:128px;height:128px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name: </label>
                        <div class="col-sm-10">
                            <p class="form-control-static">{{  Auth::user()->name }}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Date of Birth:</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">{{  Auth::user()->dob }}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email id:</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">{{  Auth::user()->email }}</p>
                        </div>
                    </div>
                </form>
                <div class="col-md-10 col-md-offset-3">
                    <a href="profile/edit" class="btn btn-default"> Edit Profile</a>
                </div>
            </div>
        </div>
    </div>
@stop