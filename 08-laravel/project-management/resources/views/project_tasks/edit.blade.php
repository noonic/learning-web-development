@extends('app')

@section('content')

    <h1>Edit Task</h1>

    <hr />
    <div class="col-md-10 col-md-offset-1">
        {!! Form::model($task, ['method' => 'PATCH', 'url' => route('tasks_update', ['projectId' => $projectId , 'id' => $task['id'] ]) ]) !!}
        {!! Form::hidden('project_id', $projectId) !!}

        @include('projects._form')

        {!! Form::close() !!}
    </div>
@stop