<?php
    $messagesSuccess = Helper::messagesDisplay('messages_success');
    $messagesError = Helper::messagesDisplay('messages_error');
?>
<?php if($messagesSuccess !== null || $messagesError !== null) { ?>
    <div class="container">

        <!-- Success Messages -->
        <?php if($messagesSuccess !== null) { ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <?php foreach($messagesSuccess as $message) { ?>
                    <p><?php echo $message; ?> </p>
                <?php } ?>
            </div>
        <?php } ?>

        <!-- Error Messages -->
        <?php if($messagesError !== null) { ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <?php foreach($messagesError as $message) { ?>
                    <p><?php echo $message; ?> </p>
                <?php } ?>
            </div>
        <?php } ?>

    </div>
<?php } ?>