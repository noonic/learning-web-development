<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Task;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;



class ProjectTasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function lists($projectId)
    {
        $userId = Auth::user()->id;
        $tasks = Task::where('project_id', '=', $projectId)->where('created_by','=',$userId)->get();
        return view('project_tasks.list', compact('tasks'))->with('projectId',$projectId);
    }

    public function add($projectId)
    {
        return view('project_tasks.add')->with('projectId', $projectId);
    }

    public function edit($projectId, $id)
    {
        $task = Task::findOrFail($id);
        return view('project_tasks.edit',compact('task'))->with('projectId', $projectId);
    }

    public function save(Request $request, $projectId)
    {

        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['project_id'] = $projectId;
        Task::create($input);
        return redirect('projects/'.$projectId.'/tasks');
    }

   public function update( Request $request, $projectId, $id ) {
        $input = $request->all();
        $input['created_by']= Auth::user()->id;
        $input['project_id'] = $projectId;
        $task = Task::where('id','=',$id)->where('project_id','=',$projectId)->first();
        $task->update($input);
        return redirect('projects/'.$projectId.'/tasks');
    }

   public function delete($projectId, $id) {
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect('projects/'.$projectId.'/tasks');
    }
}