<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once('common/head.php'); ?>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Project Management</a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="profile.php">Profile View</a> </li>
            <li><a href="index.php">Logout</a> </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form action="profile.php">
                <h2>Please change required fields</h2>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" placeholder="Previous Name">
                </div>

                <div class="form-group">
                    <label for="dob">Date of Birth:</label>
                    <input type="date" class="form-control" id="dob" placeholder="Previous Date of birth">
                </div>

                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" placeholder="Previous Email">
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Previous Password">
                </div>

                <div class="form-group">
                    <label for="conpword">Password</label>
                    <input type="password" class="form-control" id="conpword" placeholder="Confirm Password">
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php include_once('common/footer.php')?>
</body>
</html>