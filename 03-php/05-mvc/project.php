<?php
require_once('common/config.php');

$view = '/views/project/';
$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ( $action){
    case 'add':
        $view .= 'add';
        break;

    case 'edit':
        $view .= 'edit';
        break;

    default:
        $view .= 'list';
        break;

}

include_once(DEPLOY_PATH . $view . '.php');