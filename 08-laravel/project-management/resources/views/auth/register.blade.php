@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form method="POST" action="/auth/register" name="form" enctype="multipart/form-data">
                {!! csrf_field() !!}
          <!--    <input type="hidden" name="change_password_status" id="change_password_status" value="<?php// echo Helper::sessionGet('user')['id'] ? 0 : 1; ?>"> -->

                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ old('name') }}">
                </div>

                <div class="form-group">
                    <label for="dob">Date of Birth:</label>
                    <input type="date" class="form-control" id="dob" name="dob" placeholder="Enter date of birth in YYYY-MM-DD format" value="{{ old('dob') }}">
                    <p id="p2"></p>
                </div>

                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}" />
                    <span id="user-availability-status"></span>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password(>6 characters and <20 characters)"  autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="confirm_password">Re-enter password</label>
                    <input type="password" class="form-control" id="confirm_password" name="password_confirmation" placeholder="Confirm password"  autocomplete="off">
                </div>

                <button type="submit" name="submit" class="btn btn-default" >Submit</button>
            </form>
        </div>
    </div>
</div>
@stop

