<?php
final class Helper {
    
    public static function redirect($path = '') {
        $path = ($path == '') ? 'index.php' : $path;
        header('Location: '.URL_WEB.$path);
    }
    
}