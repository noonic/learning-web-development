<?php

$students = array(
    array(
        'name' => 'lannister',
        'age' => 21,
        'year' => 1994,
        'members' => array(
            'tyrion',
            'cersei',
            'tywin'
        )
    ),

    array(
        'name' => 'lannister1',
        'age' => 21,
        'year' => 1994,
        'members' => array(
            'tyrion1',
            'cersei1',
            'tywin1'
        )
    ),

    array(
        'name' => 'lannister2',
        'age' => 21,
        'year' => 1994,
        'members' => array(
            'tyrion2',
            'cersei2',
            'tywin2'
        )
    )
);
?>

<html>
<body>
    <table>
        <thead>
            <th>Name </th>
            <th>Age </th>
            <th>Year </th>
            <th colspan= 3 > Members</th>
        </thead>

        <tbody>
            <?php foreach($students as $value) { ?>
                <tr>
                    <td> <?php echo $value['name'] ?> </td>
                    <td> <?php echo  $value['age'] ?> </td>
                    <td> <?php echo $value['year'] ?> </td>
                    <?php foreach( $value['members'] as $mem) {?>
                        <td> <?php echo $mem   ?> </td>
                    <?php } ?>
                </tr>
            <?php }      ?>
        </tbody>
    </table>
</body>
</html>