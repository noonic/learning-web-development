<html>
<head>
    <?php include_once PATH_DEPLOY.'views/common/head_view.php'; ?>
</head>
<body>
    <?php include_once PATH_DEPLOY.'views/common/header_view.php'; ?>
        
    <div id="content">
        <h2>Edit Employee</h2>

        <form method="post" action="<?php URL_WEB; ?>index.php?action=save">
            <input type="hidden" name="employee_id" id="employee_id" value="<?php echo $viewData['employee']['id']; ?>"/>
                <table>
                    <tr>
                        <td>Employee Name</td>
                        <td><input type="text" name="employee_name" id="employee_name" value="<?php echo $viewData['employee']['name']; ?>"/></td>
                    </tr>

                    <tr>
                        <td colspan="2"><button type="submit">Save</button></td>
                    </tr>
                </table>
        </form>
    </div>
</body>
</html>