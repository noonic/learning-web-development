<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(PATH_DEPLOY.'views/common/head.php'); ?>
</head>

<body>
    <!-- Header -->
    <?php include_once(PATH_DEPLOY.'views/common/header.php')?>

    <!-- Messages -->
    <?php include_once(PATH_DEPLOY.'views/common/messages.php')?>

    <!-- Content -->
    <div class="container">
        <h1>
            Projects

            <a href="<?php echo URL_WEB.'projects.php?action=add'; ?>" class="btn btn-default pull-right">Add</a>
        </h1>

        <hr/>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th colspan="2" class="text-center">Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php if(count($viewData['projects']) > 0) { ?>
                    <?php foreach($viewData['projects'] as $i => $project) { ?>
                        <tr>
                            <td><?php echo ($i+1); ?></td>
                            <td><a href="<?php echo URL_WEB.'project_tasks.php?project_id=' . ($i+1); ?>"> <?php echo $project['title']; ?></a></td>
                            <td><?php echo $project['description']; ?></td>
                            <td><?php echo (Helper::statusDisplay( $project['status'] )); ?></td>
                            <td><?php echo $project['created_at']; ?></td>
                            <td><?php echo $project['updated_at']; ?></td>
                            <td><a href="<?php echo URL_WEB.'projects.php?action=edit&id='.$project['id']; ?>">Edit</a></td>
                            <td><a href="<?php echo URL_WEB.'projects.php?action=delete&id='.$project['id']; ?>" onclick="return App.confirmDelete();">Delete</a></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="8" align="center">No data found.</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

    <!-- Footer -->
    <?php include_once(PATH_DEPLOY.'views/common/footer.php')?>
</body>
</html>