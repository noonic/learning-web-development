<!DOCTYPE html>
<html>
<head>
    <title>PHP</title>
</head>
<body>
    <a href="variables.php">Variables</a>
    <br />

    <a href="operators.php">Operators</a>
    <br />

    <a href="conditional_statements.php">Conditional Statements</a>
    <br />

    <a href="loops.php">Loops</a>
    <br />

    <a href="objects.php">Objects</a>
    <br />

    <a href="date.php">Date</a>
    <br />

    <a href="multidimensional_array.php">Multidimensional Array</a>
    <br />

    <a href="session.php">Session</a>
    <br />

    <a href="cookie.php">Cookie</a>
    <br />
</body>
</html>