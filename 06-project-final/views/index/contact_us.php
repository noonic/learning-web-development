<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(PATH_DEPLOY.'views/common/head.php'); ?>
    <title>Contact Us</title>
</head>

<body>
<!-- Header -->
<?php include_once(PATH_DEPLOY.'views/common/header.php')?>

<!-- Content -->
<div class="container">
    <h1>Contact Us</h1>

    <hr/>

    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    </p>
</div>

<!-- Footer -->
<?php include_once(PATH_DEPLOY.'views/common/footer.php')?>
</body>
</html>