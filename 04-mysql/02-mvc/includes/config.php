<?php

// Paths
define('PATH_DEPLOY', $_SERVER['DOCUMENT_ROOT']. '/04-mysql/02-mvc/');
define('URL_WEB', 'http://' . $_SERVER['HTTP_HOST'] . '/04-mysql/02-mvc/');

// Database
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', '1234');
define('DB_DATABASE', 'mvc');