<?php
$id = isset($viewData['tasks']['id']) ? $viewData['tasks']['id'] : 0;
$projectId = isset($viewData['tasks']['project_id']) ? $viewData['tasks']['project_id'] : $_GET['project_id'] ;
$title = isset($viewData['tasks']['title']) ? $viewData['tasks']['title'] : '';
$description = isset($viewData['tasks']['description']) ? $viewData['tasks']['description'] : '';
$status = isset($viewData['tasks']['status']) ? intval($viewData['tasks']['status']) : 0;
?>

<form action="<?php echo URL_WEB.'project_tasks.php?action=save'; ?>" method="post" onsubmit="return App.validationActions.projectsAndTasks();">
    <input type="hidden" name="id" value="<?php echo $id; ?>" />
    <input type="hidden" name="project_id" value="<?php echo $projectId; ?>" />

    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $title; ?>" />
    </div>

    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control" name="description" id="description" placeholder="Description" value="<?php echo $description; ?>">
    </div>

    <div class="form-group">
        <label for="status">Status</label>
        <select class="form-control" name="status" id="status">
            <option value="0" <?php echo ($status === 0) ? 'selected="selected"' : '' ?>>Incomplete</option>
            <option value="1" <?php echo ($status === 1) ? 'selected="selected"' : '' ?>>Completed</option>
        </select>
    </div>

    <hr/>

    <button type="submit" class="btn btn-success">Submit</button>
</form>