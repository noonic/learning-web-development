var App = {
    validationRules: {
        name: /^[A-Za-z ]{3,20}$/,
        title: /^[A-Za-z0-9 ]{3,20}$/,
        date: /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/,
        email: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
        password: /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/,
    },

    validationActions: {

        user: function() {
            var page = $('#page').val();
            var name = $('#name').val();
            var dob = $('#dob').val();
            var email = $('#email').val();
            var password = $('#password').val();
            var confirmPassword = $('#confirm_password').val();
            var changePasswordStatus = $('#change_password_status').val();
            var message = '';

            var validated = true;
            var focusElement = false;

            if( ! App.validationRules.name.test(name)) {
                message = message.concat("Invalid name!\n");
                validated = false;
                if(!focusElement) {
                    focusElement = $('#name');
                }
            }

            if(!App.validationRules.date.test(dob)) {
                message = message.concat("Invalid date of birth\n");
                validated = false;
                if(!focusElement) {
                    focusElement = $('#dob');
                }
            }

            if(!App.validationRules.email.test(email)) {
                message = message.concat("Invalid email \n") ;
                validated = false;
                if(!focusElement) {
                    focusElement = $('#email');
                }
            }

            // If profile edit page then check password
            if(changePasswordStatus == 1) {
                if (!App.validationRules.password.test(password)) {
                    message = message.concat("Invalid password. Enter valid password of more than 6 characters \n");
                    validated = false;
                    if (!focusElement) {
                        focusElement = $('#password');
                    }
                }

                if ((password) !== (confirmPassword)) {
                    message = message.concat("The re entered password does not match");
                    validated = false;
                    if (!focusElement) {
                        focusElement = $('#confirm_password');
                    }
                }
            }

            if(validated) {
                return true;
            }

            alert(message);
            focusElement.focus();
            return false;
        },

        projectsAndTasks: function() {
            var title = $('#title').val();
            if( ! App.validationRules.title.test(title)) {
                alert("Invalid title \n");
                return false;
            } else {
                return true;
            }
        },

        emailAvailability: function() {
            $('#email').on('change', function() {
                var email = $(this).val();
                if(email.length > 3) {
                    $('#user-availability-status').html('checking...');

                    $.ajax({
                        url: URL_WEB+'index.php?action=email_availability',
                        method: 'POST',
                        data: {email: email},
                        dataType: 'JSON',
                        success: function(response) {
                            $('#user-availability-status').html(response.message);
                        },
                        error: function(response) {
                            alert('Sorry, there was some server error.');
                        }
                    });
                }
            });
        }
    },

    confirmDelete : function() {
        return confirm('Are you sure you want to delete?');
    },

    togglePassword: function() {
        $('#password-inputs-control').on('click', function () {
            $('#password-inputs').toggle();
            var newValue = ($('#change_password_status').val() == 0) ? 1 : 0;
            $('#change_password_status').val(newValue);
        });
    }
};
