<?php

$t = 16;

if ( $t < 16) {
    echo "Have a good day <br>" ;
} else {
    echo "Have a good night <br>" ;
    }

$z = 20;

if ( $z < "20") {
    echo "Have a good day <br>" ;
} else if( $z > "16") {
    echo "Have a good night <br>" ;
}

$favcolor = "red";

switch ($favcolor) {
    case "red":
        echo "Your favorite color is red!";
        break;
    case "blue":
        echo "Your favorite color is blue!";
        break;
    case "green":
        echo "Your favorite color is green!";
        break;
    default:
        echo "Your favorite color is neither red, blue, nor green!";
}