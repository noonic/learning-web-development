<?php
require_once('includes/config.php');

spl_autoload_register(function ($class) {
    include PATH_DEPLOY.'classes/'.$class.'.php';
});

$viewData = array();
$viewData['sitename'] = 'MVC';

register_shutdown_function(function() {
    Database::closeDatabaseConnection();
});