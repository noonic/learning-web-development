<?php
$message ="";
require_once('common/connection_create.php');
if(isset($_GET['id'])) {
    $id = $_GET['id'];
    $sqlQuery = "SELECT * FROM projects WHERE id=" . $id;
    $result = ($connection->query($sqlQuery));
    $row = $result->fetch_assoc();

    if (isset($_POST['submit'])) {
        $title = $connection->escape_string(isset($_POST['title']) ? $_POST['title'] : '');
        $description = $connection->escape_string($_POST['description']);
        $status = $connection->escape_string($_POST['status']);
        $createdBy = $connection->escape_string($_POST['created_by']);

        $sqlQuery = "UPDATE projects SET title= '".$title."', description= '".$description."', created_by=".$createdBy.", status=". $status." WHERE id=". $id;
        if ($connection->query($sqlQuery)) {
            $message = "Edit successful";
            header('Refresh: 3; URL=03-table_select.php');
        } else {
            $message = "Unsuccessful<br>" . $connection->error;
        }
    }
} else {
    header('Refresh: 0; URL=03-table_select.php');
}
require_once('common/connection_close.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title> Add Project</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width="device-width",initial-scale=1">

    <!--Custom CSS-->
    <link rel="stylesheet" href="css/style.css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="">Project Management</a>
            </div>
        </div>
    </nav>


    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>?id=<?php echo $_GET['id'];?>" method="post" >
                    <p><?php echo $message; ?></p>
                    <h2>Edit the required fields:</h2>

                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input type="text" class="form-control" id="title" name="title" value=<?php echo $row['title'] ?>>
                    </div>

                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea class="form-control" id="description" name="description"> <?php echo $row['description'] ?> </textarea>
                    </div>

                    <div class="form-group">
                        <label for="createdby">Created by</label>
                        <input type="text" class="form-control" id="createdby" name="created_by" value="<?php echo $row['created_by'] ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select id="status" class="form-control" name="status" >
                            <option value=0 <?php if($row['status'] === 0 ) {echo("selected");}?>>Incomplete</option>
                            <option value=1 <?php if($row['status'] === 1 ) {echo("selected");}?>>Completed</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-default" name="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-muted text-center">Custom Footer</p>
        </div>
    </footer>
    </body>
</html>