<footer class="footer">
    <hr/>

    <div class="container">

        <p class="text-muted text-center">
            &copy; <?php echo date('Y'); ?> Project Management.
            &bull;
            <a href="<?php echo URL_WEB.'index.php?action=about'; ?>">About</a>
            &bull;
            <a href="<?php echo URL_WEB.'index.php?action=contact'; ?>">Contact</a>
        </p>

    </div>
</footer>