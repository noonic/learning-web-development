<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(PATH_DEPLOY.'views/common/head.php'); ?>
    <title>Login</title>
</head>

<body>
<!-- Header -->
<?php include_once(PATH_DEPLOY.'views/common/header.php')?>

<!-- Messages -->
<?php include_once(PATH_DEPLOY.'views/common/messages.php')?>

<!-- Content -->
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <form class="form-signin" action="<?php echo URL_WEB.'index.php?action=login_check';?>" method="POST">
                <h2 class="form-signin-heading">Please login</h2>

                <label for="email" class="sr-only">Username</label>
                <input type="text" id="email" class="form-control" name="email" placeholder="Email id" required autofocus>

                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>

                <br/>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div>
    </div>
</div>

<?php include_once('views/common/footer.php')?>
</body>
</html>