<!DOCTYPE html>
<html lang="en">
<head>
    <title> Profile View </title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width="device-width",initial-scale=1">

    <!--Custom CSS-->
    <link rel="stylesheet" href="css/style.css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script></head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Project Management</a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="project_add.php">Add project+</a></li>
            <li><a href="profile.php">Profile View</a> </li>
            <li><a href="index.php">Logout</a> </li>
        </ul>
    </div>
</nav>

<div class="container">
    <table class="table table-hover">
        <caption class="text-center"><h3>Projects</h3></caption>

        <thead>
        <tr>
            <th rowspan="2" width="7%">Serial No.</th>
            <th rowspan="2">Title</th>
            <th rowspan="2">Description</th>
            <th rowspan="2">Created By</th>
            <th rowspan="2">Status</th>
            <th colspan="2" class="text-center">Actions</th>
        </tr>

        <tr>
            <th> Edit </th>
            <th> Delete</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>1.</td>
            <td ><a href="project_tasks.php" >ABC</a></td>
            <td></td>
            <td></td>
            <td></td>

            <td><a href="project_edit.php">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>

        <tr>
            <td>2.</td>
            <td ><a href="project_tasks.php" >DEF</a></td>
            <td></td>
            <td></td>
            <td></td>

            <td><a href="project_edit.php">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>

        <tr>
            <td>3.</td>
            <td ><a href="project_tasks.php" >GHI</a></td>
            <td></td>
            <td></td>
            <td></td>

            <td><a href="project_edit.php">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>

        <tr>
            <td>4.</td>
            <td ><a href="project_tasks.php" >JKL</a></td>
            <td></td>
            <td></td>
            <td></td>

            <td><a href="project_edit.php">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>

        <tr>
            <td>5.</td>
            <td ><a href="project_tasks.php" >MNO</a></td>
            <td></td>
            <td></td>
            <td></td>
            <td><a href="project_edit.php">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>

        <tr>
            <td>6.</td>
            <td ><a href="project_tasks.php" >PQR</a></td>
            <td></td>
            <td></td>
            <td></td>

            <td><a href="project_edit.php">Edit</a> </td>
            <td><a href="">Delete</a> </td>
        </tr>
        </tbody>
    </table>
</div>

<?php include_once('common/footer.php')?>
</body>
</html>