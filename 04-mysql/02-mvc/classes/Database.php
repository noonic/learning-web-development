<?php
class Database {

    protected static $connection;

    public function __construct() {
        $this->connect();
    }

    protected function connect() {
        if(!isset(self::$connection)) {
            self::$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
        }

        if(self::$connection === false) {
            return false;
        }

        return self::$connection;
    }

    protected function query($query) {
        $result = self::$connection->query($query);

        return $result;
    }

    protected function select($query) {
        $rows = array();
        $result = $this->query($query);
        if($result === false) {
            return false;
        }

        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    protected function error() {
        return self::$connection->error;
    }

    protected function quote($value) {
        return "'" . self::$connection->real_escape_string($value) . "'";
    }

    public function closeDatabaseConnection() {
        if(!self::$connection === false) {
            self::$connection->close();
        }
    }
}