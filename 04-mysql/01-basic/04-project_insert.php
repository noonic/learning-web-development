<!DOCTYPE html>
<html lang="en">
<head>
    <title> Add Project</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width="device-width",initial-scale=1">

    <!--Custom CSS-->
    <link rel="stylesheet" href="css/style.css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
   <?php
    $message = "";
   if( isset($_POST['submit'])) {
       require_once('common/connection_create.php');

       $title = $connection->escape_string($_POST['title']);
       $description = $connection->escape_string($_POST['description']);
       $status = $connection->escape_string($_POST['status']);
       $createdBy = $connection->escape_string($_POST['created_by']);

       $sqlQuery = "INSERT INTO projects (title, description, created_by, status) VALUES ( '$title', '$description', $createdBy, $status)";

       if ($connection->query($sqlQuery)) {
           $message = "Insert successful";
       } else {
           $message = "Unsuccessful";
       }
       require_once('common/connection_close.php');
   }
    ?>
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="03-table_select.php">Project Management</a>
            </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <p><?php echo $message; ?></p>
                    <h2>Enter the following fields:</h2>

                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Title">
                    </div>

                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea class="form-control" name="description"  id="description" placeholder="Description"></textarea>
                    </div>


                    <div class="form-group">
                        <label for="created_by">Created by</label>
                        <input type="text" class="form-control" name="created_by" id="created_by" >
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value=0 >Incomplete</option>
                            <option value=1 >Completed</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-default" name="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-muted text-center">Custom Footer</p>
        </div>
    </footer>
</body>
</html>