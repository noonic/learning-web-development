<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
#use Symfony\Component\Console\Helper;

use App\Project;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function lists() {
        $projects = Project::where('created_by','=',Auth::user()->id)->get();
        return view('projects.list', compact('projects'));
    }

    public function add(){
        return view('projects.add');
    }

    public function edit($projectId) {
        $project = Project::findOrFail($projectId);
        return view('projects.edit',compact('project'));
    }

    public function save(Request $request ) {
        $input = $request->all();
        $input['created_by']= Auth::user()->id;
        Project::create($input);
        return redirect('projects');
    }

    public function update(Request $request, $projectId){

        $project = Project::findOrFail($projectId);
        $input = $request->all();
        $input['created_by']= Auth::user()->id;
        $project->update($input);
        return redirect('projects');
    }

    public function delete($projectId) {
        $project = Project::findOrFail($projectId);
        $tasks = Task::where('project_id', '=', $projectId);
        $project->delete();
        $tasks->delete();
        return redirect('projects');
    }
}
