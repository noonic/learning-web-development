<?php
require_once('common/connection_create.php');

//Query to create the table projects
$sqlQuery = "CREATE TABLE projects(
    id INT AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL,
    description TEXT,
    created_by INT,
    status INT,
    PRIMARY KEY(id))";

    if($connection->query($sqlQuery) === TRUE) {
        echo "Table created successfully";
    } else {
        echo "<br>Unable to create table<br>". $connection->error;
    }

require_once('common/connection_close.php');