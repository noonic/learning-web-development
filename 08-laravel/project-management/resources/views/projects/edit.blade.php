@extends('app')

@section('content')

    <h1>Edit project</h1>

    <hr />
    <div class="col-md-10 col-md-offset-1">
        {!! Form::model($project, ['method' => 'PATCH','url' => route('projects_update', ['projectId' => $project['id']]) ] ) !!}

        @include('projects._form')

        {!! Form::close() !!}
    </div>

@stop