<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['about','contact']]);

    }

    public function login(){
        return view('auth.login');
    }

    public function profile(){
        return view('index.profile_view');
    }
    public function profile_edit(){
        return view('index.profile_edit');
    }
    public function contact(){
        return view('static.contact');
    }
    public function about(){
        return view('static.about');
    }

    public function edit(Request $request){
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        //Profile pic upload
        $file = $request->file('photo');
        $destinationPath = 'uploads';

        if($file) {

            $extension = $file->getClientOriginalExtension();
            $filename = $id . '-' . date("Y-m-d-h-i-s") . '.' . $extension;
            $uploadSuccess = $file->move($destinationPath, $filename);
            $filename = $destinationPath.'/'.$filename;

        } else if(Auth::user()->photo) {
            $filename = Auth::user()->photo ;
        } else {

            $filename = $destinationPath.'/default.jpg';

        }
        $input['photo'] = $filename;

        //Update with changes and redirect to profile view
        $user->update($input);
        return view('index.profile_view');

    }

}
