<?php

require 'vendor/autoload.php';

// My data
$user = array(
    'id' => 1,
    'name' => 'John',
    'email' => 'john@mayer.com',
    'places' => array(
        'city' => 'Mumbai',
        'state' => 'Maharashtra',
        'country' => 'India',
    )
);

// Debugging using print_r php function
print_r($user);

// Debugging using KINT
d($user);