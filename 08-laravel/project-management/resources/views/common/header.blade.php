<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            @if(Auth::check())
                <a class="navbar-brand" href="/projects">Project Management</a>
            @else
                <a class="navbar-brand" href="/auth/login">Project Management</a>
            @endif

        </div>

        <ul class="nav navbar-nav navbar-right">


            @if(Auth::check())
                <li>
                    <a href="/profile">
                        <img src="{{ Auth::user()->photo }}" alt="profile image" style="height: 20px;" class="img-circle"/>
                        {{ Auth::user()->name }}
                    </a>
                    <li><a href="/projects">Projects</a></li>
                    <li><a href="/auth/logout">Logout</a></li>
                </li>
                @else
                <li><a href="/auth/register">Signup</a></li>
            @endif
        </ul>
    </div>
</nav>