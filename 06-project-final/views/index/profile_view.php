<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(PATH_DEPLOY.'views/common/head.php'); ?>
    <title>View profile</title>
</head>

<body>
    <!-- Header -->
    <?php include_once(PATH_DEPLOY.'views/common/header.php')?>

    <!-- Messages -->
    <?php include_once(PATH_DEPLOY.'views/common/messages.php')?>

    <!-- Content -->
    <div class="container">
        <h1>
            Profile view

            <a href="<?php echo URL_WEB.'index.php?action=profile_edit' ?>" class="btn btn-default pull-right">Edit</a>
            <a href="<?php echo URL_WEB.'projects.php?user_id='.$_SESSION['user_id'] ?>" class="btn btn-default pull-right">View projects</a>
        </h1>

        <hr/>

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-4">
                    <form class="form-horizontal">
                        <div class="form-group" >
                            <label class="col-sm-2 control-label">Profile Picture:</label>

                            <div class="col-sm-10">
                                <img src="<?php echo URL_IMAGES.$viewData['profile']['photo'];?>" alt="Profile pic" style="width:128px;height:128px;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Name: </label>
                            <div class="col-sm-10">
                                <p class="form-control-static"><?php echo $viewData['profile']['name']; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Date of Birth:</label>
                            <div class="col-sm-10">
                                <p class="form-control-static"><?php echo $viewData['profile']['dob']; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email id:</label>
                            <div class="col-sm-10">
                                <p class="form-control-static"><?php echo $viewData['profile']['email']; ?></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <!-- Footer -->
    <?php include_once(PATH_DEPLOY.'views/common/footer.php')?>
</body>
</html>