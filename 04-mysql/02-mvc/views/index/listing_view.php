<html>
<head>
    <?php include_once PATH_DEPLOY.'views/common/head_view.php'; ?>
</head>
<body>
    <?php include_once PATH_DEPLOY.'views/common/header_view.php'; ?>
    <div id="content">
        <h2>Employees List</h2>

        <table width="100%">
            <thead>
                <tr>
                    <th> Id </th>
                    <th> Name </th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($viewData['employees'] as $employee) { ?>
                    <tr>
                        <td><?php echo $employee['id']; ?></td>
                        <td>
                            <a href="<?php echo URL_WEB; ?>index.php?action=edit&id=<?php echo $employee['id']; ?>"><?php echo $employee['name']; ?></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</body>
</html>