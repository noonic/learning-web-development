

@extends('app')

@section('content')
    <a href="{{\Illuminate\Support\Facades\URL::route('projects_add')}}" class="btn btn-default pull-right">Add project  +</a>
    <h2 align="center">Project List</h2>

    <table class="table table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Description</th>
                <th>Status</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th colspan="2" class="text-center">Actions</th>
            </tr>
        </thead>

        <tbody>
            @if(count($projects) > 0)
                @foreach($projects as $project)
                    <tr>
                        <td> {{ $project->id }} </td>
                        <td><a href="{{\Illuminate\Support\Facades\URL::route('tasks_lists', [ 'projectId' => $project->id ])}}">{{ $project->title }}</a></td>
                        <td>{{ $project->description }}</td>
                        <td>{{ \App\helper::statusDisplay($project->status) }}</td>
                        <td>{{ $project->created_at }}</td>
                        <td>{{ $project->updated_at }}</td>
                        <td><a href="{{\Illuminate\Support\Facades\URL::route('projects_edit', ['projectId' => $project->id])}}">Edit</a></td>
                        <td><a href="{{\Illuminate\Support\Facades\URL::route('projects_delete', ['projectId' => $project->id])}}">Delete</a></td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8" align="center">No data found.</td>
                </tr>
            @endif
        </tbody>
    </table>
@stop