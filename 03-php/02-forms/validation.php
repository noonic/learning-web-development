<html>
<body>
<?php

$nameErr = $emailErr = $genderErr = "";
$name = $email = $description = $gender = "";

if($_SERVER["REQUEST_METHOD"] === "POST") {
    if(empty($_POST['name']) ){
        $nameErr = "Name is required";
    } else {
        $name = testInput($_POST["name"]);
    }

    if(empty($_POST["email"]) ){
        $emailErr = "Email is required";
    } else {
        $email = testInput($_POST["email"]);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
        }
    }

    if (empty($_POST["gender"])) {
        $genderErr = "Gender is required";
    } else {
        $gender = testInput($_POST["gender"]);
    }
}

function testInput($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>

    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <table>
            <tr>
                <th>Name:</th>
                <td><input type="text" name="name" value="<?php echo $name; ?>" autofocus> <?php echo $nameErr;?> </td>
            </tr>

            <tr>
                <th>E-mail id:</th>
                <td><input type="email" name="email" value="<?php echo $email; ?>"><?php echo $emailErr;?></td>
            </tr>

            <tr>
                <th>Description:</th>
                <td><textarea cols="30" rows="5" name="description"  ></textarea></td>
            </tr>
            <tr>
                <th>Gender:</th>
                <td>
                    <input type="radio" value="female" name="gender" >Female
                    <input type="radio" value="male" name="gender">Male
                    <?php echo $genderErr; ?>
                </td>
            </tr>

            <tr>
                <td></td>
                <td><input type="submit" value="submit"></td>
            </tr>
        </table>
    </form>

    <p>Name:<?php echo $name ; ?> <br></p>
    <p>Email id:<?php echo $email ; ?> <br></p>
    <p>Description :<?php echo $description ; ?> <br></p>
    <p>Gender:<?php echo $gender ; ?> <br></p>
</body>
</html>