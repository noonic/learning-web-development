<?php
$x = 1;

echo "Using while<br>";
while ( $x <= 5 ) {
    echo $x++ ."<br>";
}

echo "<br>Using do-while<br>";
do {
    echo $x ."<br>";
} while( $x-- > 0 );


echo "<br>Using for<br>";
for ($x = 7 ; $x > 0 ;$x-- ) {
    echo $x ." <br>";
}

$arr=array("hey","how","you","doin'?" );

echo "<br>Using for-each<br>";
foreach($arr as $value) {
    echo "$value <br>";
}