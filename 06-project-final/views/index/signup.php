<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once(PATH_DEPLOY.'views/common/head.php'); ?>
    <title>Sign Up</title>
</head>
<body>
    <!-- Header -->
    <?php include_once(PATH_DEPLOY.'views/common/header.php')?>

    <?php include_once(PATH_DEPLOY.'views/common/messages.php')?>
    <!-- Content -->
    <div class="container">
        <h1>
            Sign Up

            <a href="<?php echo URL_WEB.'index.php'; ?>" class="btn btn-default pull-right">Back</a>
        </h1>

        <hr/>

        <?php include_once(PATH_DEPLOY.'views/index/_form.php')?>
    </div>

    <!-- Footer -->
    <?php include_once('views/common/footer.php')?>
</body>
</html>