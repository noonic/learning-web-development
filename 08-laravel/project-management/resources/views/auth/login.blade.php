@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form  class="form-signin" method="POST" action="/auth/login">
                    {!! csrf_field() !!}
                    <h2 class="form-signin-heading">Please login</h2>

                    <label for="email" class="sr-only">Username</label>
                    <input type="text" id="email" class="form-control" name="email" placeholder="Email id" value="{{ old('email') }}" required autofocus>

                    <label for="inputPassword" class="sr-only">Password</label>
                    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>

                    <br/>

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                </form>
            </div>
        </div>
    </div>
@stop