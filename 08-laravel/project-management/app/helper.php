<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class helper extends Model
{
   public static function statusDisplay($status) {

       $statusMessage = (intval($status) === 0) ? 'Incomplete' : 'Complete';
       return $statusMessage;

   }
}
