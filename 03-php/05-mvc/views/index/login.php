<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once('views/common/head.php'); ?>
</head>

<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <?php include_once('views/common/header.php'); ?>

        <ul class="nav navbar-nav navbar-right">
            <li><a href=" <?php echo WEB_PATH;?>index.php?action=signup"><span class="glyphicon glyphicon-user"></span>Signup</a> </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-3 col-md-offset-5">
            <form class="form-signin" action="<?php echo WEB_PATH;?>project.php?action=add">
                <h2 class="form-signin-heading">Please login</h2>

                <label for="uname" class="sr-only">Username</label>
                <input type="text" id="uname" class="form-control" placeholder="Username" required autofocus>

                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>

                <div class="checkbox">
                    <label><input type="checkbox" value="remember-me"> Remember me</label>
                </div>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div>
    </div>
</div>

<?php include_once('views/common/footer.php')?>
</body>
</html>