<?php
$id = isset($viewData['profile']['id']) ? $viewData['profile']['id'] : 0;
$photo = isset($viewData['profile']['photo']) ? $viewData['profile']['photo'] : DEFAULT_USER_IMAGE;
$name = isset($viewData['profile']['name']) ? $viewData['profile']['name'] : '';
$dob = isset($viewData['profile']['dob']) ? $viewData['profile']['dob'] : '';
$email = isset($viewData['profile']['email']) ? $viewData['profile']['email'] : '';
$password = isset($viewData['profile']['password']) ? $viewData['profile']['password'] : '';
$confirmPassword = isset($viewData['profile']['password']) ? $viewData['profile']['password'] : '';
?>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form action="<?php echo URL_WEB.'index.php?action=save'; ?>" method="post" onsubmit="return App.validationActions.user();" name="form" enctype="multipart/form-data">
                <input type="hidden" name="id" id="id" value="<?php echo $id; ?>" />
                <input type="hidden" name="page" id="page" value="<?php $page = Helper::sessionGet('user')['id'] ? 'profile_edit' : 'signup'; echo $page?>" />
                <input type="hidden" name="change_password_status" id="change_password_status" value="<?php echo Helper::sessionGet('user')['id'] ? 0 : 1; ?>">

                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>">
                </div>

                <div class="form-group">
                    <label for="dob">Date of Birth:</label>
                    <input type="date" class="form-control" id="dob" name="dob" placeholder="Enter date of birth in YYYY-MM-DD format" value="<?php echo $dob; ?>">
                    <p id="p2"></p>
                </div>

                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $email; ?>" />
                    <span id="user-availability-status"></span>
                </div>

                <div class="form-group" >
                    <label for="photo">Profile Picture:</label>
                    <?php if($photo) { ?>
                        <br />
                        <img src="<?php echo URL_IMAGES.$photo; ?>" alt="Profile pic" style="width:128px;height:128px;">
                    <?php } ?>
                    <br />
                    <input type="file" name="photo" id="photo" />
                </div>

                <button id="password-inputs-control" class="btn btn-default" type="button" <?php if($page == 'signup'){ echo 'style="display:none"';}  ?>> Change password</button>

                <div id="password-inputs" <?php if($page == 'profile_edit'){ echo 'style="display:none"'; }?>>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password(>6 characters and <20 characters)" <?php if($page == 'profile_edit'){ echo 'value=""'; }?> autocomplete="off">
                        <p id="p4"></p>
                    </div>

                    <div class="form-group">
                        <label for="confirm_password">Re-enter password</label>
                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm password" <?php if($page == 'profile_edit'){ echo 'value=""'; }?> autocomplete="off">
                        <p id="p5"></p>
                    </div>
                </div>

                <button type="submit" name="submit" class="btn btn-default" >Submit</button>

            </form>
        </div>
    </div>
</div>

<script>
    $(function () {
        App.validationActions.emailAvailability();

        App.togglePassword();
    });
</script>