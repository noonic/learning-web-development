<?php

// Paths
define('PATH_DEPLOY', $_SERVER['DOCUMENT_ROOT']. '06-project-final/');
define('PATH_UPLOADS', $_SERVER['DOCUMENT_ROOT']. '06-project-final/static/images/uploads/');
define('URL_WEB', 'http://' . $_SERVER['HTTP_HOST'] . '/06-project-final/');

// Paths for the static stuff
define('URL_CSS','http://'. $_SERVER['HTTP_HOST'] . '/06-project-final/static/css/' );
define('URL_JS', 'http://'.$_SERVER['HTTP_HOST'] . '/06-project-final/static/js/' );
define('URL_IMAGES','http://'. $_SERVER['HTTP_HOST'] . '/06-project-final/static/images/' );

// Constants
define('DEFAULT_USER_IMAGE','uploads/default.jpg' );

// Database
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', '1234');
define('DB_DATABASE', 'learn');