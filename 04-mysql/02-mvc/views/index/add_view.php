<html>
<head>
    <?php include_once PATH_DEPLOY.'views/common/head_view.php'; ?>
</head>
<body>
    <?php include_once PATH_DEPLOY.'views/common/header_view.php'; ?>

    <div id="content">
        <h2>Add New Employee</h2>

        <form method="post" action="<?php URL_WEB; ?>index.php?action=save">
            <table>
                <tr>
                    <td>Employee Name</td>
                    <td><input type="text" name="employee_name" id="employee_name" value=""/></td>
                </tr>

                <tr>
                    <td colspan="2"><button type="submit">Save</button></td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>