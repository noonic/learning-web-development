@extends('app')

@section('content')

    <a href="{{\Illuminate\Support\Facades\URL::route('tasks_add',['projectId' => $projectId])}}" class="btn btn-default pull-right">Add Task  +</a>
    <h2 align="center">Task List</h2>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Description</th>
            <th>Status</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th colspan="2" class="text-center">Actions</th>
        </tr>
        </thead>

        <tbody>
        @if(count($tasks) > 0)
            @foreach($tasks as $task)
                <tr>
                    <td>{{ $task->id }}</td>
                    <td>{{ $task->title }}</td>
                    <td>{{ $task->description }}</td>
                    <td>{{ \App\helper::statusDisplay($task->status) }}</td>
                    <td>{{ $task->created_at }}</td>
                    <td>{{ $task->updated_at }}</td>
                    <td><a href="{{\Illuminate\Support\Facades\URL::route('tasks_edit', ['projectId' => $projectId, 'id' => $task->id ])}}">Edit</a></td>
                    <td><a href="{{\Illuminate\Support\Facades\URL::route('tasks_delete', ['projectId' => $projectId, 'id' => $task->id ])}}">Delete</a></td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8" align="center">No data found.</td>
            </tr>
        @endif


        </tbody>
    </table>
@stop