<?php
$server_name = "localhost";
$username = "root";
$password = "1234";

//Creating the connection
$connection = new mysqli($server_name,$username,$password);

//Check connection
if($connection->connect_error) {
    die  ("Error in attempt to connect" .$connection->connect_error);
}

$sqlQuery = "CREATE DATABASE project_management";
if($connection->query($sqlQuery) === TRUE) {
    echo "Database created successfully";
} else {
    echo "Unable to create database";
}

require_once('common/connection_close.php');