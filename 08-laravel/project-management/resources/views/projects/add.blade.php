@extends('app')

@section('content')

    <h1>Add new project</h1>


    <div class="col-md-10 col-md-offset-1">
        {!! Form::open(['url' => 'projects']) !!}

        @include('projects._form')

        {!! Form::close() !!}
    </div>

@stop